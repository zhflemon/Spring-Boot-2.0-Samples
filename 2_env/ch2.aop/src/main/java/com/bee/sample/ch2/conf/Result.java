package com.bee.sample.ch2.conf;

import lombok.Getter;
import lombok.Setter;

public class Result<T> {

	// error_code 状态值：0 极为成功，其他数值代表失败
	@Getter
	@Setter
	private Integer code;

	// error_msg 错误信息，若status为0时，为success
	@Getter
	@Setter
	private String message;

	// content 返回体报文的出参，使用泛型兼容不同的类型
	@Getter
	@Setter
	private T data;

	@Override
	public String toString() {
		return "Result{" + "Code=" + code + ", Message='" + message + '\'' + ", Data=" + data + '}';
	}
}
