package com.bee.sample.ch2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bee.sample.ch2.conf.ExceptionEnum;
import com.bee.sample.ch2.conf.ExceptionHandle;
import com.bee.sample.ch2.conf.Result;
import com.bee.sample.ch2.conf.ResultUtil;

@Controller
public class HelloworldController {

	@RequestMapping("/sayhello.html")
	public @ResponseBody String say() {
		return "hello world";
	}

	@RequestMapping("/say.html")
	public @ResponseBody String say(String name) {
		return "hello world " + name;
	}

	@Autowired
	private ExceptionHandle exceptionHandle;

	/**
	 * 返回体测试
	 * 
	 * @param name
	 * @param pwd
	 * @return
	 */
	@RequestMapping(value = "/getResult", method = RequestMethod.GET)
	public Result<?> getResult(@RequestParam("name") String name) {
		Result<?> result = ResultUtil.success();
		try {
			if (name.equals("zzp")) {
				result = ResultUtil.success("SUCCESS");
			} else if (name.equals("pzz")) {
				result = ResultUtil.error(ExceptionEnum.USER_NOT_FIND);
			} else {
				@SuppressWarnings("unused")
				int i = 1 / 0;
			}
		} catch (Exception e) {
			result = exceptionHandle.exceptionGet(e);
		}
		return result;
	}
}
