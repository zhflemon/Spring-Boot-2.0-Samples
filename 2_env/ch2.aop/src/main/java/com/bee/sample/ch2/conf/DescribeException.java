package com.bee.sample.ch2.conf;

public class DescribeException extends RuntimeException {

	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 2361311247803669922L;
	private Integer code;

	/**
	 * 继承exception，加入错误状态值
	 * 
	 * @param exceptionEnum
	 */
	public DescribeException(ExceptionEnum exceptionEnum) {
		super(exceptionEnum.getMsg());
		this.code = exceptionEnum.getCode();
	}

	/**
	 * 自定义错误信息
	 * 
	 * @param message
	 * @param code
	 */
	public DescribeException(String message, Integer code) {
		super(message);
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
