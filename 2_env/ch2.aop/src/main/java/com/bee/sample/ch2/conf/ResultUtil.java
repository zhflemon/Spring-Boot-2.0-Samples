package com.bee.sample.ch2.conf;

public class ResultUtil {

	/**
	 * 返回成功，传入返回体具体出參
	 * 
	 * @param object
	 * @return
	 */
	public static Result<Object> success(Object object) {
		Result<Object> result = new Result<Object>();
		result.setCode(100);
		result.setMessage("SUCCESS");
		result.setData(object);
		return result;
	}

	/**
	 * 提供给部分不需要出參的接口
	 * 
	 * @return
	 */
	public static Result<Object> success() {
		return success(null);
	}

	/**
	 * 自定义错误信息
	 * 
	 * @param code
	 * @param msg
	 * @return
	 */
	public static Result<?> error(Integer code, String msg) {
		Result<?> result = new Result<Object>();
		result.setCode(code);
		result.setMessage(msg);
		result.setData(null);
		return result;
	}

	/**
	 * 返回异常信息，在已知的范围内
	 * 
	 * @param exceptionEnum
	 * @return
	 */
	public static Result<?> error(ExceptionEnum exceptionEnum) {
		Result<?> result = new Result<Object>();
		result.setCode(exceptionEnum.getCode());
		result.setMessage(exceptionEnum.getMsg());
		result.setData(null);
		return result;
	}
}
